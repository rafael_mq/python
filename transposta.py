def transposta(lista):

    tr = [[0]*len(lista) for x in range(len(lista[0]))]

    for i in range(len(lista)):
        for j in range(len(lista[i])):
            tr[j][i] = lista[i][j]
    return tr


grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]


tr = transposta(grid)
for i in range(len(tr)):
    print(tr[i])