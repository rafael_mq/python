#! python3
# pw.py - An insecure password locker program.

PASSWORDS = {'bb': 'Du3GZ5', 'siga': 'Pe048402184@', 'email': 'pe048402184', 'fb': 'zeref93'}

import sys
if len(sys.argv) < 2:
    print('Usage: python pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1]      # first command line arg is the account name