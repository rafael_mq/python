import random
secret = random.randint(1,20)
print('I am thinking of a number between 1 and 20.')

for i in range(1,7):
	guess = int(input('Take a guess.\n'))
	if guess < secret :
		print('Guess higher')
	elif guess > secret:
		print('Guess lower')
	else:
		break

if guess == secret:
	print('You guessed just right!')
else:
	print('You are out of tries')

